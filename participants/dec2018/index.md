---
layout: people
---
### Held at AIMS South Africa, Muizenberg from 26&nbsp;November to 7&nbsp;December 2018.

![2018 Participants](participants.jpg)

***

{% assign people = site.data.dec2018 %}{% if people.size > 0%}{% assign people = people | sort %}{% for p in people %}{% assign part = p[1] %}{% if part.status == 'complete' %}{% include faculty.md profile=part %}
{% endif %}{% endfor %}{% else %}No participants!{% endif %}